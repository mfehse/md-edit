# md-edit

## thanx to

this script is taken in advance by [vuejs.org](https://vuejs.org/examples/).

## install ..

start a terminal, goto your favoured directory and type

```bash
$ git clone https://bitbucket.org/mfehse/md-edit && cd md-edit
$ npm install
```

## .. and go

start your browser and run [md-edit/index.html](http://localhost/md-edit/index.html).
