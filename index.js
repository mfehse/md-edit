(function(){
	'use strict';

	new Vue({
		el: '#editor',
		data: {
			input: '# hello'
		},
		filters: {
			marked: marked
		}
	});
})();
